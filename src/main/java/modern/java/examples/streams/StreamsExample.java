package modern.java.examples.streams;

import modern.java.examples.model.Student;
import modern.java.examples.repository.StudentRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsExample {

    public static void main(String[] args) {
        // Create a stream with the of method.
        Stream<Integer> integerStream = Stream.of(1, 2, 3);

        // Obtain a stream from a collection.
        List<Integer> integerList = List.of(1, 2, 3);
        Stream<Integer> integerStream1 = integerList.stream();

        List<Integer> listOutOfTheStream = integerStream1.collect(Collectors.toList());
        List<Integer> listOutOfTheStream2 = integerStream.collect(Collectors.toList());

        // Streams are lazy
        Stream.of(1, 2).map(num -> {
            System.out.println("Lazy in map - " + num);
            return num + 1;
        }).collect(Collectors.toList());

        // Sorting start
        Comparator<Student> studentComparator = (o1, o2) -> {
            if (o1.getAge() > o2.getAge()) {
                return 1;
            } else if(o1.getAge() < o2.getAge()) {
                return -1;
            }
            return 0;
        };

        StudentRepository studentRepository = new StudentRepository();

        List<Student> sortedStudents = studentRepository.findAll().stream()
                .sorted((s1, s2) -> studentComparator.compare(s2, s1))
                .collect(Collectors.toList());

        System.out.println(sortedStudents);

        // Sorting done

        Student oldestStudent = sortedStudents.stream()
                .max((s1, s2) -> studentComparator.compare(s1, s2))
                .orElse(null);
        System.out.println(oldestStudent);

        Integer minInt = integerList.stream().min(Integer::compare).orElse(null);
        System.out.println(minInt);
    }
}
