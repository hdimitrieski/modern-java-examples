package modern.java.examples.model;

public class Student {
  private Integer id;
  private Integer age;

  public Student() {
  }

  public Student(Integer id, Integer age) {
    this.id = id;
    this.age = age;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  @Override
  public String toString() {
    return "Student ID: " + this.id + ", Age: " + this.age;
  }
}
