package modern.java.examples.lambda;

import java.util.function.*;

@FunctionalInterface
interface MyFunction<T, R> {
  R execute(T param);
}

// We can define our own Functional Interfaces.
@FunctionalInterface
interface TriFunction {
  Integer sum(Integer a, Integer b, Integer c);
}

@FunctionalInterface
interface TriConsumer<T, U, V> {
  void apply(T a, U b, V c);
}

public class LambdaExamples {

  // Represents an operation to be performed on a single input argument
  static Consumer<String> stringWriter = s -> System.out.println("String: " + s);
  static Consumer<Integer> integerWriter = i -> System.out.println("Integer: " + i);

  // Function - Accept one argument and produce a result.
  public void functionExample() {
    // TODO impl
    // Accept one argument and produce a result.
    Function<Integer, String> intToString;

    String intToStringResult = "";
    stringWriter.accept("function - intToString:" + intToStringResult);

    Function<Integer, Integer> increment;

    Integer integerExample = 0;
    stringWriter.accept("function - increment" + integerExample);
  }

  // Accept one argument and produce boolean.
  public static void predicateExample() {
    Predicate<Integer> myPredicate = (num) -> num == 0;

    System.out.println(myPredicate.test(1));

  }

  // Produce a result of a given type, but don't accept arguments
  public static void supplierExample() {
    // TODO impl
    Supplier<String> stringSupplier = () -> "a string";

    System.out.println(stringSupplier.get());

  }

  public static void functionWithMoreArgsExample() {
    // TODO impl
    BiFunction<Integer, Integer, Integer> add = (firstInt, secondInt) -> {
      System.out.println("Invoke add(BiFunction) with: " + firstInt + ", " + secondInt);

      return firstInt + secondInt;
    };
    TriFunction add3 = (first, second, third) -> first + second + third;

    System.out.println(add.apply(1, 2));
    System.out.println(add3.sum(1, 2, 3));

  }

  // We can pass functions as arguments
  public static void passFunctionExample(Function<Integer, Integer> fn) {
    // TODO impl
    System.out.println("Passing lambda as parameter to a function: " + fn.apply(1));
  }

  public static Integer aMethod(Integer param) {
    return param + 2;
  }

  public static void main(String[] args) {
    Runnable zeroParameterLambda = () -> {
      System.out.println("No parameters");
      System.out.println("No parameters 2");
    };
    Consumer<String> oneParameterLambda = (param) -> System.out.println("One parameter" + param);
    BiConsumer<String, Integer> twoParameterLambda = (strParam, intParam) -> System.out.println("Two parameter");

    MyFunction<Integer, String> myFunction = (param) -> {
      System.out.println("Invoking myFucntion with " + param);
      return param.toString();
    };

    zeroParameterLambda.run();
    oneParameterLambda.accept("1");
    twoParameterLambda.accept("1", 23);
    String result = myFunction.execute(2);
    System.out.println(result);

    predicateExample();
    supplierExample();

    Function<Integer, Integer> aLabmda = LambdaExamples::aMethod;
    passFunctionExample(aLabmda);
  }
}
