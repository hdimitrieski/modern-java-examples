package modern.java.examples.optional;

class Customer2 {
  // It is optional (Can be null)
  private MemberCard2 memberCard;

  public Customer2() {
  }

  public Customer2(MemberCard2 memberCard) {
    this.memberCard = memberCard;
  }

  public MemberCard2 getMemberCard() {
    // Because this value is optional we can return Optional.
    return memberCard;
  }
}

class MemberCard2 {
  private final Integer points;

  public MemberCard2(Integer points) {
    this.points = points;
  }

  public Integer getPoints() {
    return points;
  }
}

class DiscountService2 {
  public String getDiscount(Customer2 customer) {
    /*
     * Because we are working with Optional, we will avoid if-else and we would have much more readable code.
     * */
    Integer discount = getDiscountPercentage(customer.getMemberCard());
    if (discount != null) {
      return toDiscountText(discount);
    } else {
      return "";
    }
  }

  private Integer getDiscountPercentage(MemberCard2 card) {
    /*
     * We should avoid passing null to methods.
     * if (card == null) return null; // empty()
     *
     * Also instead of null we can return Optional. And with that we will tell exactly that we can return empty value.
     * We will explicitly tell the reader of the code that the result might be empty. If we return null, and the
     * reader doesn't expect that the method can return null, NullPointerException will be thrown in some cases.
     * */
    if (card.getPoints() > 50) {
      return 5;
    } else if (card.getPoints() > 20) {
      return 3;
    }
    return null;
  }

  private String toDiscountText(Integer discountPercentage) {
    return "Discount: " + discountPercentage;
  }
}

public class AvoidNullExample {
  public static void main(String[] args) {
    DiscountService2 discountService = new DiscountService2();

    System.out.println(discountService.getDiscount(new Customer2(new MemberCard2(60))));
    System.out.println(discountService.getDiscount(new Customer2(new MemberCard2(30))));
    System.out.println(discountService.getDiscount(new Customer2(new MemberCard2(10))));
    System.out.println(discountService.getDiscount(new Customer2()));
  }
}
