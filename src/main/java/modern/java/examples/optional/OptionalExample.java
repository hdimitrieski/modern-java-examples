package modern.java.examples.optional;

import java.util.List;
import java.util.Optional;

import static java.util.Optional.ofNullable;
import static java.util.Optional.of;

class User {
  private Address address;

  public User() {
  }

  public User(Address address) {
    this.address = address;
  }

  public Address getAddress() {
    return address;
  }
}

class Address {
  private String street;

  public Address() {
  }

  public Address(String street) {
    this.street = street;
  }

  public String getStreet() {
    return street;
  }
}

class User2 {
  private Address2 address;

  public User2() {
  }

  public User2(Address2 address) {
    this.address = address;
  }

  public Optional<Address2> getAddress() {
    return ofNullable(address);
  }
}

class Address2 {
  private String street;

  public Address2() {
  }

  public Address2(String street) {
    this.street = street;
  }

  public Optional<String> getStreet() {
    return ofNullable(street);
  }
}

public class OptionalExample {

  private static String getStreet(User user) {
    /*
     * If we wanted to make sure we won’t hit the exception in this example, we would need to do explicit checks
     * for every value before accessing it. And that results in many null checks.
     * */

    if (user != null) {
      if (user.getAddress() != null) {
        if (user.getAddress().getStreet() != null) {
          return user.getAddress().getStreet().toUpperCase();
        }
      }
    }

    return "";
  }

  private static String getStreetNullSafe(User2 user) {
    // user, address and street are optional
    String street = "";
    Optional<String> aList;
    // TODO implement
    return street;
  }

  private static String getStreetOptional(User2 user) {
    return user.getAddress()
            .flatMap(address2 -> address2.getStreet())
            .orElse("");
  }

  public static Optional createEmptyOptional() {
    return Optional.empty();
  }

  public static Integer ofNullableExample() {
    Integer someInt = 2;

    return Optional.ofNullable(someInt).orElse(0);
  }

  public static Optional<String> ofNullableExample2() {
    String someString = "text";
    return Optional.ofNullable(someString);
  }

  public static String ofExample() {
    String someString = "";
    return someString;
  }

  public static Optional<String> ofNPE() {
    String someString = "Some value...";
    return Optional.of(someString);
  }


  public static void main(String[] args) {
    User user = new User(new Address("street 2"));
    System.out.println(getStreet(user));

    System.out.println("Optional.empty(): " + createEmptyOptional());

    System.out.println("Optional.of - " + ofNPE());

    System.out.println("Optional.ofNullable(T): " + ofNullableExample());
    System.out.println("Optional.ofNullable(T): " + ofNullableExample2());
    System.out.println("Optional.of(T): " + ofNullableExample());

    String street2 = getStreetOptional(new User2(new Address2("street1")));
    System.out.println("Street - optional: " + street2);
  }
}
