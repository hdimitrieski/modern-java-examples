package modern.java.examples.optional;

import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

class Customer {
  // It is optional (Can be null)
  private MemberCard memberCard;

  public Customer() {
  }

  public Customer(MemberCard memberCard) {
    this.memberCard = memberCard;
  }

  public Optional<MemberCard> getMemberCard() {
    // Because this value is optional we can return Optional.
    return ofNullable(memberCard);
  }
}

class MemberCard {
  private final Integer points;

  public MemberCard(Integer points) {
    this.points = points;
  }

  public Integer getPoints() {
    return points;
  }
}

class DiscountService {
  public String getDiscount(Customer customer) {
    /*
    * Because we are working with Optional, we will avoid if-else and we would have much more readable code.
    * If we don't use Observable we would have something like this:
    * Integer discount = getDiscountPercentage(customer.getMemberCard())
    * if (discount != null) {
    *   return toDiscountText(discount)
    * } else {
    *   return ""
    * }
    * */

    return customer.getMemberCard()
        .flatMap(this::getDiscountPercentage)
        .map(this::toDiscountText)
        .orElse("");
  }

  private Optional<Integer> getDiscountPercentage(MemberCard card) {
    /*
    * We should avoid passing null to methods.
    * if (card == null) return null; // empty()
    *
    * Also instead of null we can return Optional.
    * */
    if (card.getPoints() > 50) {
      return of(5);
    } else if (card.getPoints() > 20) {
      return of(3);
    }
    return empty();
  }

  private String toDiscountText(Integer discountPercentage) {
    return "Discount: "+ discountPercentage;
  }
}

public class AvoidNullExampleSolution {
  public static void main(String[] args) {
    DiscountService discountService = new DiscountService();

    System.out.println(discountService.getDiscount(new Customer(new MemberCard(60))));
    System.out.println(discountService.getDiscount(new Customer(new MemberCard(30))));
    System.out.println(discountService.getDiscount(new Customer(new MemberCard(10))));
    System.out.println(discountService.getDiscount(new Customer()));
  }
}
