package modern.java.examples.repository;

import modern.java.examples.model.Student;

import java.util.Arrays;
import java.util.List;

public class StudentRepository {
  private List<Student> students;

  public StudentRepository() {
    this.students = Arrays.asList(
        new Student(1, 24),
        new Student(2, 19),
        new Student(3, 20)
    );
  }

  public Student findById(Integer id) {
    return students.stream().filter(student -> student.getId().equals(id)).findFirst().orElse(null);
  }

  public List<Student> findAll() {
    return students;
  }

}
