package modern.java.examples.intermediateoperations;

import modern.java.examples.model.Student;
import modern.java.examples.repository.StudentRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PeekExample {
  public static void main(String[] args) {
    StudentRepository studentRepository = new StudentRepository();

    Integer[] studentIds = {1, 2, 3};

    /*
     * The first peek() is used to set the age of the students to 10.
     * The second peek() is used to print the students.
     * */
    List<Student> students = Stream.of(studentIds)
            .map(studentRepository::findById)
            .peek(student -> student.setAge(10))
            .peek(System.out::println)
            .collect(Collectors.toList());

    System.out.println(students);
  }
}
