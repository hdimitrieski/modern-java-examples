package modern.java.examples.intermediateoperations;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class FlatMapExample {
  public static void main(String[] args) {
    List<List<String>> nestedStrings = Arrays.asList(
        Arrays.asList("Hello", "World"),
        Arrays.asList("Test", "FlatMap")
    );
    System.out.println(nestedStrings);

    /*
     * We are able to convert the Stream<List<String>> to Stream<String> – using the flatMap().
     * */
    List<String> flatStrings = nestedStrings.stream()
            .flatMap(listOfStrings -> listOfStrings.stream())
            .collect(Collectors.toList());

    System.out.println(flatStrings);
  }
}
