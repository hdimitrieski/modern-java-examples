package modern.java.examples.intermediateoperations;

import modern.java.examples.model.Student;
import modern.java.examples.repository.StudentRepository;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilterExample {
  public static void main(String[] args) {
    StudentRepository studentRepository = new StudentRepository();

    /*
     * We first filter out null references for invalid student ids and then again apply a filter to only keep
     * students older than 20.
     * */
    List<Student> source = studentRepository.findAll();
    List<Student> result = source
            .stream()
            .filter(student -> student.getAge() > 20)
            .collect(Collectors.toList());

    System.out.println(source);
    System.out.println(result);
  }
}
