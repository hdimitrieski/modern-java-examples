package modern.java.examples.intermediateoperations;

import modern.java.examples.model.Student;
import modern.java.examples.repository.StudentRepository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapExample {

  public static void main(String[] args) {
    StudentRepository studentRepository = new StudentRepository();

    Integer[] studentIds = {4, 1, 2, 3, 5};

    /*
     * We obtain an Integer stream of student ids from an array. Each Integer is passed to the method
     * studentRepository::findById(Integer id) – which returns the corresponding Student object.
     * This forms a Student stream.
     * */

    Stream<Integer> streamOfIds = Stream.of(studentIds);
    Stream<Student> streamOfStudents = streamOfIds.map(studentId -> studentRepository.findById(studentId));
    Stream<Student> filteredStreamOfStudents = streamOfStudents.filter(student -> student != null);
    List<Student> listOfFilteredStudents = filteredStreamOfStudents.collect(Collectors.toList());
    System.out.println(listOfFilteredStudents);

    List<Student> students = Stream.of(studentIds)
            .map(studentId -> studentRepository.findById(studentId))
            .filter(student -> student != null)
            .collect(Collectors.toList());

    System.out.println(students);
  }
}
