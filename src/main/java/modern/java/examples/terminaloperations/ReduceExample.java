package modern.java.examples.terminaloperations;

import java.util.Arrays;
import java.util.List;

public class ReduceExample {
  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3);

    /*
     * Sum of all integers.
     * The reduce method takes a BinaryOperator as a parameter.
     *
     * Here acc variable is assigned 0 as the initial value and i is added to it.
     * */
    Integer sum = numbers.stream()
            .map(x -> x * x)
            .reduce(0, (acc, i) -> acc + i);

    System.out.println(sum);
  }
}
