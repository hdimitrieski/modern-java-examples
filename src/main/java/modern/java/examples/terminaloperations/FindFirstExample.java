package modern.java.examples.terminaloperations;

import modern.java.examples.model.Student;
import modern.java.examples.repository.StudentRepository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

public class FindFirstExample {
  public static void main(String[] args) {
    StudentRepository studentRepository = new StudentRepository();

    /*
     * The first student with the age greater than 20 is returned. If no such student exists, then null is returned.
     * */

    List<Student> listOfStudents = studentRepository.findAll();
    Optional<Student> result = listOfStudents.stream().findFirst();
    Student firstStudent = result.orElse(null);

    System.out.println(firstStudent);
  }
}
