package modern.java.examples.terminaloperations;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class ForEachExample {
  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3);

    // Multiply each integer with itself and then print it out in the console
    Stream<Integer> integerStream = numbers.stream();

    integerStream.forEach((anInt) -> System.out.println(anInt));

  }
}
