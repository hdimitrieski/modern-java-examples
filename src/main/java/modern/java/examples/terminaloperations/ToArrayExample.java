package modern.java.examples.terminaloperations;

import modern.java.examples.model.Student;
import modern.java.examples.repository.StudentRepository;

import java.util.stream.Stream;

public class ToArrayExample {
  public static void main(String[] args) {
    StudentRepository studentRepository = new StudentRepository();

    Integer[] studentIds = {1, 2};

    /*
     * Student[]::new creates an empty array of Students which is then filled with elements from the stream.
     * */
    Student[] students = Stream.of(studentIds)
        .map(studentId -> studentRepository.findById(studentId))
        .toArray(Student[]::new);

    for (int i = 0; i < students.length; i++) {
      System.out.println(students[i]);
    }

  }
}
