package modern.java.examples.homework;

import java.util.*;

import static java.util.Optional.ofNullable;

public class StudentService {
  private final Map<Long, Student> studentsById = Map.of(
      1L, new Student(1L, "Sansa", "Stark", 26, "sansa.stark@gmail.com"),
      2L, new Student(2L, "Tyrion", "Lannister", 23, "tyrion@gmail.com"),
      3L, new Student(3L, "Theon", "Greyjoy", 21, "gtheon@yahoo.com"),
      4L, new Student(4L, "Bran", "Stark", 44, "branthebroken@gmail.com"),
      5L, new Student(5L, "Tormund", "Giantsbane", 12, "tormundg@yahoo.com"),
      6L, new Student(6L, "Arya", "Stark", 51, "as@gmail.com"),
      7L, new Student(7L, "Jaime", "Lannister", 23, "ilovecersei@gmail.com"),
      8L, new Student(8L, "John", "Snow", 18, "thekingofthenorth@yahoo.com"),
      9L, new Student(9L, "Cersei", "Lannister", 21, "cl@gmail.com"),
      10L, new Student(10L, "Jorah", "Mormont", 35, "jorah84@yahoo.com")
  );

  public Optional<Student> findStudentById(Long id) {
    return ofNullable(studentsById.get(id));
  }

  public List<Student> findAllStudents() {
    return new ArrayList<>(studentsById.values());
  }
}
