package modern.java.examples.homework;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class App {
  private static Consumer<Optional<Student>> studentWriter = student -> System.out.println(
      student
          .map(Student::toString)
          .orElse("No student found")
  );
  private static Consumer<List<Student>> studentsWriter = students -> System.out.println(
      students.stream()
          .map(Student::toString)
          .collect(Collectors.joining(" - "))
  );

  private final StudentService studentService;
  private final GradeService gradeService;
  private final CourseService courseService;

  public App(StudentService studentService, GradeService gradeService, CourseService courseService) {
    this.studentService = studentService;
    this.gradeService = gradeService;
    this.courseService = courseService;
  }

  public void run() {
    // All tasks should be solved only with streams. Do not use for, while or if.
    System.out.println("App - Start");

    studentWriter.accept(findStudentByEmail(studentService.findAllStudents(), "tormundg@yahoo.com"));
    studentsWriter.accept(filterStudentsByLastName(studentService.findAllStudents(), "Stark"));
    // ...

    System.out.println("App - End");
  }

  /**
   * Task 1
   */
  public Optional<Student> findStudentByEmail(List<Student> students, String email) {
    // TODO Implement
    return null;
  }

  /**
   * Task 2
   */
  public List<Student> filterStudentsByLastName(List<Student> students, String lastName) {
    // TODO Implement
    Predicate<Student> equalEmail = student -> student.getLastName().equals(lastName);

    return null;
  }

  /**
   * Task 3
   */
  public Integer calculateSumOfGrades(List<Grade> grades) {
    // TODO implement
    return null;
  }

  /**
   * Task 4
   */
  public List<String> findAllDifferentGradeStrings(List<Grade> grades) {
    // TODO implement
    return null;
  }

  /**
   * Task 5
   * <p>
   * Returns the max grade for given course.
   *
   * @param course a course
   * @return an {@link Optional} describing max grade for a course,
   * or an empty {@link Optional} if the course doesn't contain any grades.
   */
  public Optional<Integer> findMaxGrade(Course course) {
    // TODO Implement

    return null;
  }

  /**
   * Task 6
   *
   * Returns the average grade.
   */
  public Double calculateAverageGrade(Course course) {
    // TODO Implement

    return null;
  }

  /**
   * Task 7
   *
   * Returns the student with highest grade.
   */
  public Optional<Student> findStudentWithHighestScore(Course course) {
    // TODO Implement

    return null;
  }

  /**
   * Task 8
   */
  public List<Student> findFirst3StudentsWithAgeGreaterThan25(List<Student> students) {
    // TODO implement
    return null;
  }

}
