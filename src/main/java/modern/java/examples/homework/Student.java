package modern.java.examples.homework;

public class Student {
  private final Long id;
  private final String firstName;
  private final String lastName;
  private final Integer age;
  private final String email;

  public Student(Long id, String firstName, String lastName, Integer age, String email) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
    this.email = email;
  }

  public Long getId() {
    return id;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public Integer getAge() {
    return age;
  }

  public String getEmail() {
    return email;
  }

  @Override
  public String toString() {
    return "Student: " + id + ", " + firstName + ", " + lastName + ", " + age + ", " + email;
  }

}
