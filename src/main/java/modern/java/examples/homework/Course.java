package modern.java.examples.homework;

import java.util.*;

public class Course {
  private final Long id;
  private final String name;
  private final Map<Student, Grade> gradeMap = new HashMap<>();

  public Course(Long id, String name) {
    this.id = id;
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void addStudent(Student student, Grade grade) {
    gradeMap.put(student, grade);
  }

  public List<Student> getStudents() {
    return new ArrayList<>(gradeMap.keySet());
  }

  public List<Grade> getGrades() {
    return new ArrayList<>(gradeMap.values());
  }

  public Map<Student, Grade> getGradeMap() {
    return gradeMap;
  }

  @Override
  public String toString() {
    return "Course: " + id + ", " + name;
  }
}
