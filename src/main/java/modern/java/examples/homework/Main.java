package modern.java.examples.homework;

public class Main {
  public static void main(String[] args) {
    final StudentService studentService = new StudentService();
    final GradeService gradeService = new GradeService();
    final CourseService courseService = new CourseService(studentService, gradeService);

    new App(studentService, gradeService, courseService).run();
  }
}
