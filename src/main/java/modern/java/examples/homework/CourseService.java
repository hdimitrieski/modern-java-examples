package modern.java.examples.homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

public class CourseService {
  private final Map<Long, Course> coursesById;
  private final StudentService studentService;
  private final GradeService gradeService;

  public CourseService(StudentService studentService, GradeService gradeService) {
    this.studentService = studentService;
    this.gradeService = gradeService;
    this.coursesById = createCourses();
  }

  public Optional<Course> findCourseById(Long id) {
    return ofNullable(coursesById.get(id));
  }

  public List<Course> findAllCourses() {
    return new ArrayList<>(coursesById.values());
  }

  private Map<Long, Course> createCourses() {
    List<Student> students = studentService.findAllStudents();
    List<Grade> grades = gradeService.findAllGrades();

    Course course1 = new Course(200L, "Modern Java");
    course1.addStudent(students.get(0), grades.get(0));
    course1.addStudent(students.get(1), grades.get(1));
    course1.addStudent(students.get(2), grades.get(2));
    course1.addStudent(students.get(3), grades.get(3));
    course1.addStudent(students.get(4), grades.get(4));

    Course course2 = new Course(300L, "Web Applications");
    course2.addStudent(students.get(5), grades.get(5));
    course2.addStudent(students.get(6), grades.get(6));

    Course course3 = new Course(400L, "Spring Boot");
    course3.addStudent(students.get(7), grades.get(7));
    course3.addStudent(students.get(8), grades.get(8));
    course3.addStudent(students.get(9), grades.get(9));

    return Map.of(
        200L, course1,
        300L, course2,
        400L, course3
    );
  }


}
