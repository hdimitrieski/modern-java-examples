package modern.java.examples.homework;

public class Grade {
  private final Long id;
  private final Integer score; // 0 - 100
  private final String gradeString; // A, B, C, D...

  public Grade(Long id, Integer score, String gradeString) {
    this.id = id;
    this.score = score;
    this.gradeString = gradeString;
  }

  public Long getId() {
    return id;
  }

  public Integer getScore() {
    return score;
  }

  public String getGradeString() {
    return gradeString;
  }

  @Override
  public String toString() {
    return "Grade: " + id + ", " + score + ", " + gradeString;
  }
}
