package modern.java.examples.homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

public class GradeService {

  private final Map<Long, Grade> gradesById = Map.of(
      11L, new Grade(11L, 40, "E"),
      21L, new Grade(21L, 100, "A"),
      31L, new Grade(31L, 91, "A"),
      41L, new Grade(41L, 65, "D"),
      51L, new Grade(51L, 88, "B"),
      61L, new Grade(61L, 82, "B"),
      71L, new Grade(71L, 61, "C"),
      81L, new Grade(81L, 70, "C"),
      91L, new Grade(91L, 85, "B"),
      101L, new Grade(101L, 24, "E")
  );

  public Optional<Grade> findGradeById(Long id) {
    return ofNullable(gradesById.get(id));
  }

  public List<Grade> findAllGrades() {
    return new ArrayList<>(gradesById.values());
  }
}
