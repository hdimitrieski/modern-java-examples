package modern.java.examples.comparisonbasedops;

import java.util.List;
import java.util.stream.Collectors;

public class SortedExample {
  public static void main(String[] args) {
    List<Integer> unsorted = List.of(4, 2, 1, 3);

    System.out.println(unsorted);

    /*
     * Sort ASC / DESC
     * */
    List<Integer> resultAsc = unsorted.stream().sorted().collect(Collectors.toList());

    List<Integer> resultDesc = unsorted.stream()
            .sorted((a, b) -> {
              System.out.println("Compare: " + a + ", " + b);

              return Integer.compare(b, a);
            })
            .collect(Collectors.toList());

    System.out.println(resultAsc);
    System.out.println(resultDesc);
  }
}
