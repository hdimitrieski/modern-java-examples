package modern.java.examples;

import modern.java.examples.homework.App;
import modern.java.examples.homework.CourseService;
import modern.java.examples.homework.GradeService;
import modern.java.examples.homework.StudentService;
import org.junit.Before;

public class AppTest {
  StudentService studentService;
  GradeService gradeService;
  CourseService courseService;
  App app;

  @Before
  public void init() {
    studentService = new StudentService();
    gradeService = new GradeService();
    courseService = new CourseService(studentService, gradeService);
    app = new App(studentService, gradeService, courseService);
  }

}
