package modern.java.examples;

import modern.java.examples.homework.App;
import modern.java.examples.homework.Student;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.assertj.core.api.Assertions.assertThat;


/**
 * Test for {@link App#findStudentByEmail(List, String)}.
 */
public class Task1Test extends AppTest {

  @Test
  public void shouldReturnEmpty() {
    // when
    Optional<Student> result = app.findStudentByEmail(studentService.findAllStudents(), "test@gmail.com");

    // then
    assertFalse(result.isPresent());
  }

  @Test
  public void shouldReturnAStudent() {
    // given
    Student expected = new Student(9L, "Cersei", "Lannister", 21, "cl@gmail.com");

    // when
    Optional<Student> result = app.findStudentByEmail(studentService.findAllStudents(), "cl@gmail.com");

    // then
    assertTrue(result.isPresent());
    assertThat(result.get()).isEqualToComparingFieldByField(expected);
  }
}
